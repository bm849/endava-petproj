https://trello.com/b/DuT7smEm/devops-pet-project we can use trello board to track the progress !

Pet Project:

You need to create and deliver simple application to the cloud infrastructure.
You will need to do it step by step and in the end you should be able to open the app from the internet.
To do this you need to use CI/CD tools and the gitops approach.


The Preparation:
0. Create a repo in the Endava Gitlab. (it should be a private repo) https://gitlab.com/
1. Find the application in the Internet with a frontend(website) and backend components (it may also have a DB, and maybe some other components) https://github.com/knaopel/docker-frontend-backend-db
2. Optional: Run the application on the local VM to make sure that it's working.

Dockerization:
1. Create a Dockerfile and build docker images with your app inside(one image per component). https://docs.docker.com/engine/reference/builder/
2. Run this application localy with docker or docker-compose, so you make sure that the app is working. https://docs.docker.com/compose/gettingstarted/#step-3-define-services-in-a-compose-file
3. Push the application and Dockerfiles to the repo.

Play with Docker/Cloud (CI): 
1. Get the AWS/GCP/Azure account.
2. Configure Gitlab pipeline(gitlab-ci) to build your application and push the images to the registry (`Cloud Registry` or Gitlab Registry). https://docs.gitlab.com/ee/user/packages/container_registry/ or https://cloud.google.com/container-registry/docs/pushing-and-pulling
3. Pull the image from the registry to your PC, and run the container localy to make sure that the app is working.
4. Optional: Pull the images from Cloud Shell or cloud VM.

Kubernetes:
1. Manually spin up Cloud-Native kubernetes(k8s) cluster and get access to it(try Lens app https://k8slens.dev/).
2. Create k8s manifests to deploy your containers in this cluster. Use k8s services so the app components can communicate with each other. https://kubernetes.io/docs/concepts/cluster-administration/manage-deployment/
3. Apply the manifests to the k8s cluster and make sure that the app works(you will need a k8s port-forwarding). https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/
4. Create a helm chart with the same manifests. https://helm.sh/docs/topics/charts/
5. Use Helm3 to deploy the app to the k8s cluster from your PC. https://helm.sh/docs/helm/helm_install/

Gitlab Pipelines (CD):
https://docs.gitlab.com/ee/ci/introduction/index.html#continuous-integration

1. Optional: create a new project and configure deploy steps there.
2. Configure access from the Gitlab pipeline to the k8s cluster to deploy your app by gitlab-ci. You will need a gitlab runner installed in the k8s cluster. https://docs.gitlab.com/runner/install/kubernetes.html
3. Create a pipeline job to deploy your manifests to the k8s cluster.
4. Create a pipeline job to deploy your Helm chart to the k8s cluster(another namespace).

Play with Cloud:
1. Optional: run this application on the Cloud VM and open the website from the internet. Get the temporary domain from the Cloid Provider.
2. Optional: run this application on the Cloud-Native docker and open the website from the internet. Get the temporary domain from the Cloid Provider.

Kubernetes Advanced:
1. Install nginx-ingress and cert-manager controller(you can use official helm charts). https://github.com/kubernetes/ingress-nginx/tree/main/charts/ingress-nginx and https://cert-manager.io/docs/installation/helm/
2. Configure ingress manifests for frontend service. https://kubernetes.io/docs/concepts/services-networking/ingress/
3. Get the temporary domain from the Cloid Provider or register free domain on https://www.freenom.com/ (it slow as hell, but free).
4. Configure DNS to point your domain to the cluster.
5. Open website from the internet.
